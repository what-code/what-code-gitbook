# Privacy Policy
## GitLab
What Co.DE's GitLab stores your data both online (on your host) and locally (on your device). We use the device keychain to store your credentials and an encrypted database to store all remaining data.

Every message transferred is encrypted with TLS/SSL only when the appropriate option is enabled both on the app and on your GitLab instance.